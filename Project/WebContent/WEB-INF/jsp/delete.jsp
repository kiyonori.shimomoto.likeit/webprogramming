<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>


    <div class="login-area">
    <div class="color">
    <div class="parent">
    <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
  			  <a href="Lout" class="navbar-link logout-link">ログアウト</a>
            </li>
  		  </ul>
    </div>
    </div>


    <h1 class="title">ユーザ削除確認</h1>


    <p class="text-uppercase">ログインID：${user.loginId}</p>
        <p class="text-uppercase">を本当に削除してよろしいでしょうか。</p>


          <div class="form_conf">
    <form action="Ulist" method="get">
     <input type="hidden" name="id" value="${user.id}">
        <input type="submit" name="done" value="キャンセル" id="submit_button">
        </form>
<form action="Udelete" method="post">
 <input type="hidden" name="id" value="${user.id}">
        <input type="submit" name="to_correct" value="OK" id="submit_button">
    </form>
</div>


    </div>
    </body>
</html>