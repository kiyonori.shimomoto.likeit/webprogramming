<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>

	<div class="login-area">
		<div class="color">
			<div class="parent">
				<ul class="nav navbar-nav navbar-right">
					<li class="navbar-text">${userInfo.name}さん</li>
					<li class="dropdown"><a href="Lout"
						class="navbar-link logout-link">ログアウト</a></li>
				</ul>
			</div>
		</div>


		<h1 class="title">ユーザ一覧</h1>

		<div class="container">

			<div class="text-right">
				<a href="Unew">新規登録</a>
			</div>

			<div class="login-area">

				<form class="form-signin" action="Ulist" method="post">
					<div class="form-group row">
						<label for="loginId" class="col-sm-3 col-form-label">ログインID</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="loginId"
								name="loginId">
						</div>
					</div>


					<div class="form-group row">
						<label for="user" class="col-sm-3 col-form-label">ユーザ名</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="user" name="name">
						</div>
					</div>

					<div class="form-group row">
						<label for="born" class="col-sm-3 col-form-label">生年月日</label>
						<div class="col-sm-9">
							<div class="row">
								<div class="clo-sm-5">
									<input type="date" class="form-control" id="born" name="born">
								</div>
								<div class="clo-sm-1">～</div>
								<div class="clo-sm-5">
									<input type="date" class="form-control" id="born" name="born2">
								</div>
							</div>
						</div>
					</div>

					<div class="button-wrapper">
						<input type="submit" class="btn btn-light" value="検索">
					</div>
				</form>

				<HR>


				<table class="table table-striped">
					<thead class="thead-dark">
						<tr>
							<th scope="col">ログインID</th>
							<th scope="col">ユーザ名</th>
							<th scope="col">生年月日</th>
							<th scope="col"></th>
						</tr>
					</thead>
					<tbody>

						<c:forEach var="user" items="${userList}">

							<tr>
								<td>${user.loginId}</td>
								<td>${user.name}</td>
								<td>${user.birthDate}</td>
								<td><c:choose>
										<c:when test="${userInfo.loginId == 'admin'}">
											<a class="btn btn-primary" href="Ureference?id=${user.id}">詳細</a>
											<a class="btn btn-success" href="Uupdate?id=${user.id}">更新</a>
											<a class="btn btn-danger" href="Udelete?id=${user.id}">削除</a>
										</c:when>
										<c:when test="${user.loginId == userInfo.loginId}">
											<a class="btn btn-primary" href="Ureference?id=${user.id}">詳細</a>
											<a class="btn btn-success" href="Uupdate?id=${user.id}">更新</a>
										</c:when>
										<c:otherwise>
											<a class="btn btn-primary" href="Ureference?id=${user.id}">詳細</a>
										</c:otherwise>
									</c:choose></td>
						</c:forEach>


					</tbody>
				</table>

			</div>
		</div>
	</div>
</body>
</html>