<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>


	<div class="container">

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>



		<h1 class="title">ログイン画面</h1>


		<div class="login-area">
			<form class="form-signin" action="Ulogin" method="post">
				<div class="form-group row">
					<label for="loginId" class="col-sm-3 col-form-label">ログインID</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="loginId"
							name="loginId">
					</div>
				</div>

				<div class="form-group row">
					<label for="password" class="col-sm-3 col-form-label">パスワード</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="password"
							name="password">
					</div>
				</div>


				<div class="button-wrapper">
					<button type="submit" class="btn btn-light">ログイン</button>
				</div>
			</form>
		</div>
	</div>


</body>
</html>