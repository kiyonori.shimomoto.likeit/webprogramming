<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
<div class="container">
<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	<div class="new-area">
		<div class="form-group row color ">
			<div class="col-12 row">
				<div class="button-wrapper col-9">
					<ul class="nav navbar-nav navbar-right">
						<li class="navbar-text">${userInfo.name}さん</li>
						<li class="dropdown"><a href="Lout"
							class="navbar-link logout-link">ログアウト</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<h1 class="title">ユーザー新規登録</h1>
	<div class="new-area">
		<form class="form-signin" action="Unew" method="post">
			<div class="form-group row">
				<label for="loginId" class="col-sm-3 col-form-label">ログイン</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="loginId" name="loginId">
				</div>
			</div>
			<div class="form-group row">
				<label for="pass" class="col-sm-3 col-form-label">パスワード</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="pass" name="pass">
				</div>
			</div>
			<div class="form-group row">
				<label for="pass2" class="col-sm-3 col-form-label">パスワード（確認）</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="pass2" name="pass2">
				</div>
			</div>
			<div class="form-group row">
				<label for="user" class="col-sm-3 col-form-label">ユーザ名</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="user" name="user">
				</div>
			</div>
			<div class="form-group row">
				<label for="born" class="col-sm-3 col-form-label">生年月日</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="born" name="born">
				</div>
			</div>
			<div class="button-wrapper">
				<button type="submit" class="btn btn-light">登録</button>
			</div>
		</form>
		<a href="Ulist">戻る</a>
	</div>
	</div>
</body>
</html>