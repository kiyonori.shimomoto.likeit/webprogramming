<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="login-area">
    <div class="color">
    <div class="parent">
    <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
  			  <a href="Lout" class="navbar-link logout-link">ログアウト</a>
            </li>
  		  </ul>
    </div>
    </div>



<h1 class="title">ユーザ情報詳細参照</h1>
     <div class="login-area">



          <div class="form-group row">
    <label class="col-sm-3 col-form-label">ログインID</label>
    <div class="col-sm-9">

      ${user.loginId}
    </div>
  </div>

         <div class="form-group row">
    <label class="col-sm-3 col-form-label">ユーザ名</label>
    <div class="col-sm-9">

      ${user.name}
    </div>
    </div>

    <div class="form-group row">
    <label class="col-sm-3 col-form-label">生年月日</label>
    <div class="col-sm-9">

      ${user.birthDate}
    </div>
        </div>

    <div class="form-group row">
    <label class="col-sm-3 col-form-label">登録日時</label>
    <div class="col-sm-9">

      ${user.createDate}
    </div>
        </div>

    <div class="form-group row">
    <label class="col-sm-3 col-form-label">更新日時</label>
    <div class="col-sm-9">

      ${user.updateDate}
    </div>
    </div>



         </div>

 <a href="Ulist">戻る</a>
        </div>
      </body>
</html>