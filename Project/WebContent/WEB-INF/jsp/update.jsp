<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div class="login-area">
    <div class="color">
    <div class="parent">
     <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
  			  <a href="Lout" class="navbar-link logout-link">ログアウト</a>
            </li>
  		  </ul>
    </div>
    </div>

<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

<h1 class="title">ユーザ情報更新</h1>
     <div class="login-area">
          <div class="form-group row">
    <label class="col-sm-3 col-form-label">ログインID</label>
    <div class="col-sm-9">
       ${user.loginId}
    </div>
  </div>

<form class="form-signin" action="Uupdate" method="post">
	<input type="hidden" name="id" value="${user.id}">
         <div class="form-group row">
    <label for="pass" class="col-sm-3 col-form-label">パスワード</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="pass" name="pass">
    </div>
    </div>

    <div class="form-group row">
    <label for="pass2" class="col-sm-3 col-form-label">パスワード（確認）</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="pass2" name="pass2">
    </div>
        </div>

    <div class="form-group row">
    <label for="user" class="col-sm-3 col-form-label">ユーザ名</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="user" name="user" value="${user.name}">
    </div>
        </div>

    <div class="form-group row">
    <label for="born" class="col-sm-3 col-form-label">生年月日</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="born" name="born" value="${user.birthDate}">
    </div>
    </div>

         <div class="button-wrapper">
                 <button type="submit" class="btn btn-light">更新
                </button>
            </div>
</form>
         <a href="Ulist">戻る</a>
         </div>
        </div>
      </body>
</html>