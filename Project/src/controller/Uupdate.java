package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Uupdate
 */
@WebServlet("/Uupdate")
public class Uupdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Uupdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	HttpSession session = request.getSession();
		 User Sid = (User)session.getAttribute("userInfo");

			if(Sid == null){
			response.sendRedirect("Ulogin");
		}
			else {


    	String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User user = userDao. findById(id);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("user", user);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
    }
    }

	/**

    }
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
		 protected void doPost(HttpServletRequest request, HttpServletResponse response)
					throws ServletException, IOException {
		        // リクエストパラメータの文字コードを指定
		        request.setCharacterEncoding("UTF-8");

		        String text = request.getParameter("text");

				// リクエストパラメータの入力項目を取得

				String pass = request.getParameter("pass");
				String pass2 = request.getParameter("pass2");
				String user = request.getParameter("user");
				String born = request.getParameter("born");
				String id = request.getParameter("id");



				if (!pass.equals(pass2)) {
					request.setAttribute("errMsg", "入力された内容は正しくありません");

					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
					dispatcher.forward(request, response);
					return;

				} else if (user.equals("") || born.equals("")) {
					request.setAttribute("errMsg", "入力された内容は正しくありません");

					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
					dispatcher.forward(request, response);
					return;
				}

				UserDao userDao = new UserDao();
				userDao.upDateUser(pass,user,born,id);


				if(text !=null && text.length() !=0) {
					ServletContext applicathion =this.getServletContext();
					List<User> userList= (List<User>) applicathion.getAttribute("userList");
				}


				// ユーザ一覧のサーブレットにリダイレクト
				response.sendRedirect("Ulist");
	}

}
