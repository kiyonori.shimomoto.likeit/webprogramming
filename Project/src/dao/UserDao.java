package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		String angou = Encryption(password);

		try {

			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, angou);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE login_id !='admin'";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public void signUpUser(String loginId, String password, String user, String born) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		String angou = Encryption(password);
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "INSERT INTO user(login_id, password, name, birth_date, create_date, update_date) values(?,?,?,?,NOW(),NOW())";

			// SELECTを実行し、結果表を取得
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, angou);
			pStmt.setString(3, user);
			pStmt.setString(4, born);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return;
				}
			}
		}
	}

	public User findById(String targetId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, targetId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

			return user;

			// 必要なデータのみインスタンスのフィールドに追加

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}
	public User findByLId(String targetloginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, targetloginId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

			return user;

			// 必要なデータのみインスタンスのフィールドに追加

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	public User upDateUser(String password, String user, String born, String targetId) {
		Connection conn = null;
		PreparedStatement pStmt = null;
		String angou = Encryption(password);
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "UPDATE user SET password=?, name=?, birth_date=?, update_date=NOW() WHERE id=?";

			// SELECTを実行し、結果表を取得
			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, angou);
			pStmt.setString(2, user);
			pStmt.setString(3, born);
			pStmt.setString(4, targetId);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			//クローズ処理
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public List<User> deleteUser(String targetId) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "DELETE FROM user WHERE id = ? LIMIT 1";

			// SELECTを実行し、結果表を取得

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, targetId);
			pStmt.executeUpdate();

			// 必要なデータのみインスタンスのフィールドに追加

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return userList;
	}

	public List<User> Search(String loginIdP, String nameP, String bornS, String bornF) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id != 'admin'";

			// SELECTを実行し、結果表を取得

			// SELECTを実行し、結果表を取得

			if (!loginIdP.equals("")) {
				sql += " AND login_id = ?";
			}

			if (!nameP.equals("")) {
				sql += " AND name LIKE ? ";
			}
			if (!bornS.equals("")) {
				sql += " AND birth_date >= ?";
			}

			if (!bornF.equals("")) {
				sql += " AND birth_date < ?";
			}

			PreparedStatement pStmt = conn.prepareStatement(sql);


			int count = 1;

			if (!loginIdP.equals("")) {
				pStmt.setString(count, loginIdP);
				count++;
			}

			if (!nameP.equals("")) {
				pStmt.setString(count, "%" + nameP + "%");
				count++;
			}
			if (!bornS.equals("")) {
				pStmt.setString(count, bornS);
				count++;
			}

			if (!bornF.equals("")) {
				pStmt.setString(count, bornF);
				count++;
			}

			System.out.println(sql);

			ResultSet rs = pStmt.executeQuery();

			//String sql = "SELECT * FROM user WHERE login_id != 'admin'";
			//Statement pStmt = conn.createStatement();
			//if(!loginIdP.equals("")) {
			//	sql += " AND login_id = '"+loginIdP+"'";
			//}
			//if(!nameP.equals("")) {
			//	sql += " AND name LIKE = '"+nameP+"'";
			//}
			//if(!bornS.equals("")) {
			//	sql += " AND birth_date >= '"+bornS+"'";
			//}
			//if(!bornF.equals("")) {
			//	sql += " AND AND birth_date < '"+bornF+"'";
			//}
			//System.out.println(sql);
			//ResultSet rs = pStmt.executeQuery(sql);

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			while (rs.next()) {

				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name1 = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name1, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return userList;
	}

	public static String Encryption(String password) {
		String source = password;

		Charset charset = StandardCharsets.UTF_8;

		String algorithm = "MD5";

		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		System.out.println(result);
		return result;
	}

}
